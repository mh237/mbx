---
title: "Read in Data"
author: "John Little"
date: "`r Sys.Date()`"
output:
  html_document:
    df_print: paged
---

## Load Packages

```{r message=FALSE, warning=FALSE}
library(tidyverse)
library(readxl)
library(skimr)
library(randomizr)
```

## Load Data

```{r}
oa <- read_excel("data/OA_testcase.xlsx",
                 sheet = "OA", skip = 6)
oa <-  oa %>% 
  select(-`#`)  # deselect the first column which is an excel rownames column of sequentially numbered rows

oa
```

### Skim

quick view of the data

```{r}
skim(oa)
```
 
 
## Non-Uniform Random Variate Generators

Some randmon approaches that may be worth investigation

- library(randomizr)  
- library(rvgtest)  ??

## dplyr::sample_n

to take a random sample of rows

```{r}
sample_n(oa, 3)
```

## Sampling Methods

### seeding

I've commented out the `set.seed()` function because I don't need to reproduce the groupings for this code exploration.  Going forward, any time group membershi needs to be fixed and reproducible in code, `set.seed` is necessary.

```
set.seed(250624)
```

### randomizr::simple_ra()

http://randomizr.declaredesign.org/reference/simple_ra.html

The output from simple_ra is nice, i.e. a readable factor data type.  But I cannot see how it's useful in assigning a group id into rows of a data frame.

 
```{r}
#set.seed(343)  # set for reproducibility

group_test <- simple_ra(nrow(oa), num_arms = 8)
table(group_test)
```

### base R::sample()

Here I am using base R `sample()` instead of `dplyr::sample_n()`.  The difference is in operable flexibility not in output.  That is, `sample_n()` is designed for use with an entire Tibble (dataframe), while `sample()` can be used to add a single column (in this case, the Group ID column: 'gid' group GID).


https://stackoverflow.com/questions/34028371/randomly-sample-data-frame-into-3-groups-in-r

1. create 3 subsets of randmonized groups with group IDs assigned
1. re-assemble the dataframe from the 3 subsetted dataframes

```{r}
# set.seed(7)
ss <- sample(1:3, size = nrow(oa), replace = TRUE)
oa_sub1 <- oa[ss==1,] %>% mutate(gid = 1)
oa_sub2 <- oa[ss==2,] %>% mutate(gid = 2)
oa_sub3 <- oa[ss==3,] %>% mutate(gid = 3)

oa_all <- bind_rows(oa_sub1, oa_sub2, oa_sub3) %>% 
  select(1, 9, 2:9)  # alter the order of the columns so that 'gid' is in the 2nd position

oa_all
```

#### Variation:  tidyverse overlay of Base R approach

Rewrote the base R style (above) to be more tidyverse in coding style.  The operation is the same, only easier to read.  The outcome could be the same but are randomly different due to running sample without setting the seed.  The main operation are in the first two lines of the code chunk, below.  But, to verify some of this activity, additional coding is done to prove the outcome is as expected.  You can look closer or ignore the verification coding. 

```{r}
oa_option3 <- oa %>% 
  mutate(GID = sample(1:6, nrow(oa), replace = TRUE))

oa_option3 %>% 
  select(1, 9, 2:9) 
```


##### Verifications

```{r}
oa_option3 %>% 
  group_by(GID) %>% 
  count() 
```


```{r}
oa_option3 %>% 
  select(1, 9, 2:9) %>% 
  group_by(GID, `Sample ID`) %>% 
  count() %>% 
  arrange(-n)
```






